return require('packer').startup(function(use)
	  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  use 'tpope/vim-fugitive'
  use 'airblade/vim-gitgutter'
  use 'w0rp/ale'
  use { 'ibhagwan/fzf-lua',
    -- optional for icon support
    requires = { 'kyazdani42/nvim-web-devicons' }
  }
  use 'kyazdani42/nvim-web-devicons'
  use 'tpope/vim-surround'
  use 'mkitt/tabline.vim'
  use 'tpope/vim-endwise'
  use 'morhetz/gruvbox'
  use 'sheerun/vim-polyglot'
  use 'tpope/vim-rhubarb'
  use 'junegunn/goyo.vim'
  use 'junegunn/limelight.vim'
  use 'NLKNguyen/papercolor-theme'
  use({
      "hrsh7th/nvim-cmp",
      requires = {
        { "hrsh7th/cmp-nvim-lsp" },
        { "hrsh7th/cmp-vsnip" },
        { "hrsh7th/vim-vsnip" },
     },
  })
  use({
    "scalameta/nvim-metals",
    requires = {
      "nvim-lua/plenary.nvim",
      "mfussenegger/nvim-dap",
    },
  })
  use 'habamax/vim-asciidoctor'
  use 'nvim-telescope/telescope.nvim'
  use 'nvim-telescope/telescope-fzf-native.nvim'
  use 'dracula/vim'
  use 'rhysd/git-messenger.vim'
  use 'fmoralesc/vim-pad'
  use 'sainnhe/everforest'
  use 'kien/rainbow_parentheses.vim'
--  use 'neovim/nvim-lspconfig'
  use 'preservim/nerdtree'
  use 'Xuyuanp/nerdtree-git-plugin'
  use 'ryanoasis/vim-devicons'
  use 'folke/tokyonight.nvim'
  use {
  'nvim-lualine/lualine.nvim',
  requires = { 'nvim-tree/nvim-web-devicons', opt = true }
  }
end)
