-- nmap <leader>n ]c
-- nmap <leader>p [c	

-- F3: Toggle list (display unprintable characters).
vim.keymap.set('n', '<F3>',  ':set list!<CR>')

vim.keymap.set('n', '<Tab>', ':buffer<Space><Tab>')
--vim.keymap.set('n', '<leader>q', ':Git add -p<CR>')
--vim.keymap.set('n', '<leader>w', ':Gcommit<CR>')
--vim.keymap.set('n', '<leader>e', ':Gpush<CR>')
vim.keymap.set('n', '<leader>b', ':FzfLua buffers<CR>')
vim.keymap.set('n', '<leader>fi', ':FzfLua git_files<CR>')
vim.keymap.set('n', '<leader>m', ':FzfLua marks<CR>')
vim.keymap.set('n', 'tt',  ':tabedit<Space>')
vim.keymap.set('n', '<leader>ne',  ':NERDTreeToggle<CR>')
vim.keymap.set('n', '<leader>nf',  ':NERDTreeFocus<CR>')

-- telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})

vim.keymap.set('t', '<leader><ESC>', '<C-\\><C-n>', {noremap = true})

-- nerdtree

vim.keymap.set('n', '<leader>t', ':NERDTreeToggle<CR>')
