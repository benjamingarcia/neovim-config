vim.g.nocompatible = true
vim.opt.shell= '/bin/zsh'
vim.opt.encoding='utf-8'
vim.g.t_Co=256
vim.opt.linespace=3
vim.opt.laststatus=2
vim.opt.title=true
vim.opt.number=true
vim.opt.ruler=true
vim.opt.wrap=true
vim.opt.scrolloff=3

vim.opt.ignorecase=true
vim.opt.smartcase=true
vim.opt.incsearch=true
vim.opt.hlsearch=true

vim.opt.visualbell=true
vim.g.noerrorbells=true

vim.opt.backspace='indent,eol,start'

vim.opt.hidden=true

-- https://jameschambers.co.uk/vim-typescript-slow
vim.opt.re=0

vim.opt.clipboard='unnamed'

vim.g.mapleader = ","

vim.opt.wildmenu=true
vim.opt.wildmode='full'

vim.opt.guicursor='n-v-c-i:block'
