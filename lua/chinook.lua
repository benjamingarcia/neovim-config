vim.opt.background='dark'  -- Setting light mode
-- colorscheme solarized8_light
--  colorscheme gruvbox
--   colorscheme PaperColor
-- let g:airline_theme='solarized'
-- vim.g.tokyonight_style = 'storm' --" available: night, storm
-- vim.g.tokyonight_enable_italic = 1
vim.g.everforest_enable_italic = true
-- Lua
require('lualine').setup {
  options = {
    -- ... your lualine config
    theme = 'everforest'
    -- ... your lualine config
  }
}
vim.cmd [[
  syntax enable
  colorscheme everforest
]]

